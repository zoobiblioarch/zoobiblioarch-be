import dbConnect from '../../schema/db';
import Model from '../../schema/model';
import allowCors from '../allowCors';

const handler = async (req, res) => {
  const { Paper } = Model;
  const db = await dbConnect();

  let { keywords, chronology, regions, page, pageSize, yearFrom, yearTo } =
    req.body;

  const papers = Paper.find();

  if (keywords && keywords.length > 0) papers.where('keywords').in(keywords);
  if (chronology && chronology.length > 0)
    papers.where('chronology').in(chronology);
  if (regions && regions.length > 0) papers.where('regions').in(regions);
  if (yearFrom) papers.where('year').gt(Number(yearFrom - 1));
  if (yearTo) papers.where('year').lt(Number(yearTo + 1));
  if (!page) page = 0;
  if (!pageSize) pageSize = 10;

  const count = await papers.clone().countDocuments().exec();
  const result = await papers
    .limit(pageSize)
    .skip(page * pageSize)
    .populate('keywords')
    .populate('regions')
    .populate('chronology')
    .exec();

  res.send({
    docs: result,
    totalDocs: count,
    currentPage: page,
    hasPrev: page * pageSize > 0,
    hasNext: page * pageSize < count - pageSize,
    totalPages: Math.floor(count / pageSize),
  });

  await db.connection.close();
};

module.exports = allowCors(handler);
