const dbConnect = require('./schema/db');
const Model = require('./schema/model');
const fs = require('fs');

async function migrate() {
  const db = await dbConnect();

  const { Paper, Chronology, Keyword, Region } = Model;

  const file = fs
    .readFileSync('./main_final.csv', { encoding: 'utf-8' })
    .toString();
  const data = csvToArray(file);
  let regions = new Set();
  let keywords = new Set();

  await Paper.deleteMany();
  await Region.deleteMany();
  await Keyword.deleteMany();

  for (let paper of data) {
    // const chronology = new Chronology();
    if (paper[5]) {
      paper[5].split(';').forEach((region) => regions.add(region.trim()));
    }
    if (paper[6]) {
      paper[6].split(';').forEach((keyword) => keywords.add(keyword.trim()));
    }
  }

  console.log(regions, keywords);

  for (const region of regions) {
    try {
      await new Region({ name: region }).save();
    } catch (e) {
      console.log('duplicate: ', region);
    }
  }
  for (const keyword of keywords) {
    try {
      await new Keyword({ name: keyword }).save();
    } catch (e) {
      console.log('duplicate: ', keyword);
    }
  }

  const keywordsDb = await Keyword.find({});
  const regionsDb = await Region.find({});

  const papersToSave = data.map((paper) => ({
    authors: paper[0],
    year: paper[1],
    title: paper[2],
    journal_book: paper[3],
    doi: paper[4],
    regions: regionsDb.filter((region) => paper[5]?.includes(region.name)),
    keywords: keywordsDb.filter((keyword) => paper[6]?.includes(keyword.name)),
    abstract: paper[7],
  }));

  await Paper.insertMany(papersToSave);
  db.connection.close();
  console.log('done');
}

migrate();

function csvToArray(text) {
  let p = '',
    row = [''],
    ret = [row],
    i = 0,
    r = 0,
    s = !0,
    l;
  for (l of text) {
    if ('"' === l) {
      if (s && l === p) row[i] += l;
      s = !s;
    } else if (',' === l && s) l = row[++i] = '';
    else if ('\n' === l && s) {
      if ('\r' === p) row[i] = row[i].slice(0, -1);
      row = ret[++r] = [(l = '')];
      i = 0;
    } else row[i] += l;
    p = l;
  }
  return ret;
}
